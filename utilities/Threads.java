package com.albertomartino.utilities;

public class Threads {
    public static void RandomSleep(double min, double max) throws InterruptedException {
        Thread.sleep((long)Random.Between(min, max));
    }

    public static <T extends Number> void RandomSleep(Range<T> range) throws InterruptedException {
        RandomSleep((double)range.min, (double)range.max);
    }
}
