package com.albertomartino.utilities;

public class Random {
    public static double Between(double min, double max){
        return (Math.random()*((max-min)+1))+min;
    }

    public static int Between(int min, int max){
        return (int) Between((double)min, max);
    }

    public static float Between(float min, float max) {
        return (float) Between((double)min, max);
    }

    public static long Between(long min, long max) {
        return (long) Between((double) min, max);
    }
}
