package com.albertomartino.assignements.assignement8;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/*
    Si assume che client e server si connettano sulla porta di default su local host utilizzando buffer di lettura/scrittura con capacità di default.
    I valori di default sono riferiberibili staticamente dalla classe Echo.
 */
public class Server {
    public static void main(String[] args) {
        // Apro una server socket channel sulla quale mettermi in ascolto e un selettore per recuperare le chiavi
        try(
                var serverChannel = ServerSocketChannel.open();
                var selector = Selector.open()
        ){
            // Collego la socket al local host sulla porta di default, imposto il canale come non bloccante
            serverChannel.bind(new InetSocketAddress(Echo.DEFAULT_PORT));
            serverChannel.configureBlocking(false);

            System.out.println("Echo Server started.\tBound to: " + serverChannel.getLocalAddress());

            // Registro il canale con interesse ad accettare
            serverChannel.register(selector, SelectionKey.OP_ACCEPT);

            while(true){
                // Rimuovo eventuali chiavi precedentemente restituite e tento di selezionarne di nuove
                selector.selectedKeys().clear();
                if (selector.select() == 0) { continue; }

                for (var key : selector.selectedKeys())
                {
                    /*  IsAcceptable:
                        - Accetto una connessione sulla s.socket, la configuro come non-bloccante e la registro con interesse alla lettura
                        - Se si verifica un errore di IO chiudo eventualmente il canale, deregistro la chiave e passo ad osservare la successiva
                     */
                    if (key.isAcceptable()){
                        SocketChannel client;

                        try { client = ((ServerSocketChannel) key.channel()).accept(); }
                        catch (IOException ioException) {
                            key.cancel();
                            continue;
                        }

                        try {
                            System.out.println("CONNECTED to: " + client.getRemoteAddress());
                            client.configureBlocking(false);
                            client.register(selector, SelectionKey.OP_READ, ByteBuffer.allocate(Echo.BUFFER_SIZE));
                        }
                        catch(IOException e){
                            PrintConnectionClosed(client, "accepting");
                            CloseAndUnregister(key, client);
                            continue;
                        }
                    }
                    /*  IsReadable:
                        - Recupero il messaggio (attachment) dal canale associato alla chiave quindi indico interesse alla scrittura sul canale
                        - Se raggiungo la fine dello stream, chiudo la connessione, deregistro la chiave e proseguo sulla chiave successiva
                        - Se si verifica un errore di IO,    chiudo la connessione, deregistro la chiave e proseguo sulla chiave successiva
                        - faccio flip sul buffer solo dopo averlo letto perché ho già il messaggio da inviare ma devo poterlo scrivere interamente
                     */
                    if (key.isReadable()){
                        var channel = (SocketChannel)key.channel();
                        var msg = (ByteBuffer) key.attachment();
                        try {
                            if (channel.read(msg) == -1) {
                                PrintConnectionClosed(channel, "reading (Unexpected EndOfStream reached)");
                                CloseAndUnregister(key, channel);
                                continue;
                            }
                            msg.flip();
                            channel.register(selector, SelectionKey.OP_WRITE, msg);
                        }
                        catch(IOException e){
                            CloseAndUnregister(key, channel);
                            PrintConnectionClosed(channel, "reading");
                            continue;
                        }

                    }
                    /*  IsWritable:
                        - Recupero il messaggio (attachment) dal canale associato alla chiave, lo invio sul canale,
                                     quindi preparo il buffer per una nuova lettura e indico interesse alla lettura sul canale
                        - Se si verifica un errore di IO,    chiudo la connessione, deregistro la chiave e proseguo sulla chiave successiva
                     */
                    if (key.isWritable()){
                        var channel= (SocketChannel) key.channel();
                        var message= (ByteBuffer)key.attachment();
                        try {
                            channel.write(message);
                            message.rewind();
                            var txtMessage = new String(message.array(), 0, message.limit());
                            System.out.println("\n" + txtMessage);
                            for (int i = 0; i < txtMessage.length(); i++) { System.out.print('-'); }
                            System.out.println("> " + channel.getRemoteAddress());
                            message.clear();
                            channel.register(selector, SelectionKey.OP_READ, message);
                        } catch(IOException ioException){
                            CloseAndUnregister(key, channel);
                            PrintConnectionClosed(channel, "writing");
                        }
                    }
                }
            }
        } catch( IOException e){
            e.printStackTrace();
            System.out.println("Couldn't properly launch server because of an IO error.");
        }
    }

    // Chiude la connessione con client e deregistra la chiave
    private static void CloseAndUnregister(SelectionKey key, SocketChannel client) {
        try { client.close(); } catch (IOException ioException) { ioException.printStackTrace(); }
        key.cancel();
    }

    // Stampa un messaggio di errore ad indicare che la connessione con client è stata chiusa durante l'azione indicata da state
    private static void PrintConnectionClosed(SocketChannel client, String state) {
        String from;
        try { from  = client.getRemoteAddress().toString(); }
        catch (Exception exception) { from = "[connection close]"; }
        System.out.println("ERROR: IO error while " + state + ", connection with client " + from + " is closed.");
    }
}
