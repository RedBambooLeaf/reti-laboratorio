package com.albertomartino.assignements.assignement8;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Scanner;

/*
    Si assume che client e server si connettano sulla porta di default su local host utilizzando buffer di lettura/scrittura con capacità di default.
    I valori di default sono riferiberibili staticamente dalla classe Echo.
 */
public class Client {
    public static void main(String[] args) {

        // Ottengo l'indirizzo remoto al quale connettere il client (local host con porta di default)
        var address = new InetSocketAddress(Echo.DEFAULT_PORT);
        try (
                var scanner = new Scanner(System.in);  // Utilizzerò lo scanner per leggere la linea da inviare al server
                var channel = SocketChannel.open(address)   // apro un canale all'indirizzo computato precedentemente
        ) {
            System.out.println(channel.getLocalAddress() + " connected to echo server " + channel.getRemoteAddress());
            // Preparo un buffer per gestire i dati di scrittura/lettura
            var message= ByteBuffer.allocate(Echo.BUFFER_SIZE);
            while (true) {
                try {
                    // Appena l'utente invia una linea di testo, la scrivo nel buffer che invio sul canale, quindi pulisco il buffer
                    System.out.print("$ echo ");
                    message.put(scanner.nextLine().getBytes());
                    message.flip();
                    channel.write(message);
                    message.clear();

                    // Leggo la risposta sul canale e la stampo a schermo, quindi pulisco il buffer
                    channel.read(message);
                    message.flip();
                    System.out.println(">>>>>> " + new String(message.array(), 0, message.limit()));
                    message.clear();
                }
                // Se il messaggio digitato è troppo lungo, comunico l'impossibilità di spedirlo, pulisco il buffer e attendo nuovo input
                catch (BufferOverflowException bufferOverflowException) {
                    System.out.println("ERROR: Message too long! (Max " + (Echo.BUFFER_SIZE - 1) + ")");
                    message.clear();
                }
            }

        } catch (IOException ioException) {
            System.out.println("Connection closed: some IO error occured.");
        }
    }

}
