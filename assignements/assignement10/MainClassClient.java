package com.albertomartino.assignements.assignement10;
import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

import static com.albertomartino.assignements.assignement10.Dategroup.getInputs;

/**
 * La classe WelcomeClient si unisce al gruppo e riceve, per dieci volte consecutive, data ed ora, le visualizza, quindi termina
 *
 * L’indirizzo IP del gruppo viene introdotto  da linea di comando
 * Si è scelto di utilizzare un indirizzo di default in caso di errori con quello specificato
 *
 * @author Alberto Martino
 * @version 1.0
 */
public class MainClassClient {
    /**
     * numero di volte che viene tentata una lettura prima di terminare
     */
    private static final int DEFAULT_READ_TIMES = 10;
    /**
     * indirizzo del gruppo di multicast
     */
    private final InetAddress multicastGroup;
    /**
     * porta associata all'indirizzo multicast
     */
    private final int groupPort;
    /**
     * lunghezza del messaggio da ricevere (possiamo avere questa informazione)
     */
    private final int MSG_LENGTH = 64;

    /**
     *
     * Si tenta di assegnare dei valori per inizializzare un client, quindi avvio quel client;
     * Se l'input non è valido, si tenta di assegnare valori di default, notificandolo
     * Se anche con i valori di default, l'operazione fallisce, si notifica stampando il trace
     */
    public static void main(String[] args){
        try {
            var inputData = getInputs(args, Dategroup.DEFAULT_ADDRESS, Dategroup.DEFAULT_PORT);
            if (inputData != null) { new MainClassClient(inputData.getAddress(), inputData.getPort()).start(); }
        }
        catch (UnknownHostException e) { System.err.println("unexpected error"); e.printStackTrace(); }
    }

    /**
     *
     * @param groupAddress inet address del gruppo di multicast
     * @param groupPort porta a cui associare il socket di multicast
     */
    public MainClassClient(InetAddress groupAddress, int groupPort) {
        this.multicastGroup = groupAddress;
        this.groupPort = groupPort;
    }

    /**
     * si unisce al gruppo quindi riceve e visualizza data ed ora per dieci volte consecutive
     */
    public void start(){
        try (var multicastSocket = new MulticastSocket(this.groupPort)) {
            multicastSocket.joinGroup(this.multicastGroup);
            var datagramPacket = new DatagramPacket(new byte[this.MSG_LENGTH], MSG_LENGTH);
            for (int i = 0; i < DEFAULT_READ_TIMES; i++) {
                multicastSocket.receive(datagramPacket);
                var receivedLocalDateTimeText = new String(datagramPacket.getData(), datagramPacket.getOffset(), datagramPacket.getLength());
                var receivedLocalDateTime = Dategroup.convert(receivedLocalDateTimeText);
                var date = receivedLocalDateTime.toLocalDate();
                var time = receivedLocalDateTime.toLocalTime();
                System.out.printf("%d)\tDate: %s \tTime: %s\n", i+1, date.toString(), time.toString());
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
