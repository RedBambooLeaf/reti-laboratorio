package com.albertomartino.assignements.assignement10;
import com.albertomartino.utilities.Range;
import org.apache.commons.cli.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Stabilisce valori di default e metodi statici d'interesse comune a client e server
 *
 * @author Alberto Martino
 * @version 1.0
 */
public class Dategroup {
    /**
     * indirizzo del gruppo di multicast
     */
    public final static String DEFAULT_ADDRESS = "239.255.1.3";
    /**
     * porta associata all'indirizzo di multicast
     */
    public final static int DEFAULT_PORT = 30000;
    /**
     * Range di numeri di porta accettabili
     */
    public final static Range<Integer> DEFAULT_ALLOWED_PORTRANGE = new Range<>(0, 65535);
    /**
     * il formato di data ricevuto
     */
    public static final DateTimeFormatter DEFAULT_DATETIME_FORMATTER = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    /**
     * @param localDateTimeText il local date time in formato testuale
     * @return il local date time in formato LocalDateTime
     * @throws DateTimeParseException se il parsing è fallito
     */
    public static LocalDateTime convert(String localDateTimeText) throws DateTimeParseException {
        return LocalDateTime.parse(localDateTimeText, DEFAULT_DATETIME_FORMATTER);
    }

    /**
     *
     * @param localDateTime il local date time in formato LocalDateTime
     * @return il local date time in formato testuale
     * @throws DateTimeException se si è verificato un errore durante la conversione
     */
    public static String convert(LocalDateTime localDateTime) throws DateTimeException {
        return localDateTime.format(DEFAULT_DATETIME_FORMATTER);
    }

    /**
     *
     * Salva gli input da linea di comando e, se non riesce,
     * assegna valori di default e ne dà comunicazione
     * @param args gli argomenti passati da linea di comando
     * @param defaultDategroupAddress indirizzo di default da usare in caso sia impossibile utilizzare l'input utente
     * @param defaultDategroupPort porta di default da usare in caso sia impossibile utilizzare l'input utente
     * @throws UnknownHostException se l'indirizzo specificato da user non è valido e non si riesce a risolvere neanche quello di default
     * @return la coppia dati <address: InetAddress, port: int>
     */
    public static InputData getInputs(String[] args, String defaultDategroupAddress, Integer defaultDategroupPort) throws UnknownHostException {
        // controllo argomenti linea comand
        var opts = new Options();

        // -a --address <multicast adrress>
        var optAddress = new Option("a", "address" , true, "multicast address");
        optAddress.setRequired(true);
        opts.addOption(optAddress);

        // -p --port <multicast port>
        var optPort = new Option("p", "port", true, "multicast port");
        optPort.setRequired(false);
        opts.addOption(optPort);

        var inputData = new InputData();
        try {
            var cmd = new DefaultParser().parse(opts, args);

            var hasValidPortOption = cmd.hasOption("p");
            if (hasValidPortOption) {
                try  {
                    inputData.port = Integer.parseInt(cmd.getOptionValue("p"));
                    if (inputData.port > DEFAULT_ALLOWED_PORTRANGE.max || inputData.port < DEFAULT_ALLOWED_PORTRANGE.min) {
                        throw new IllegalArgumentException();
                    }
                }
                catch (IllegalArgumentException e) {        // cattura anche NumberFormatException : IllegalFormatException
                    System.out.println("La porta specificata non è valida.\tAssegnato valore di default");
                    hasValidPortOption = false;
                }
            }
            if (!hasValidPortOption) {
                inputData.port = defaultDategroupPort;
            }

            try {
                inputData.address = InetAddress.getByName(cmd.getOptionValue("a"));
                if (!inputData.address.isMulticastAddress()) { throw new IllegalArgumentException(); }
            }
            catch (UnknownHostException | IllegalArgumentException exception) {
                System.out.println("L'indirizzo specificato non è valido.\tAssegnato valore di default");
                inputData.address = InetAddress.getByName(defaultDategroupAddress);
            }
        }
        catch (ParseException pe) {
            System.err.println("argomenti non validi");
            new HelpFormatter().printHelp("java <.class>", opts);
            return null;
        }

        return inputData;
    }

    /**
     *
     * Modella una struttura dati per l'input
     */
    public static class InputData {
        private InetAddress address;
        private Integer port;

        public InetAddress getAddress() { return address; }
        public Integer getPort() { return port; }
    }

}
