package com.albertomartino.assignements.assignement10;
import static com.albertomartino.assignements.assignement10.Dategroup.getInputs;
import java.io.IOException;
import java.net.*;
import java.time.DateTimeException;
import java.time.LocalDateTime;

/**
 * La classe MainClassServer
 *  - invia su un gruppo di multicast, ad intervalli regolari, la data e l’ora
 *  - attende tra un invio ed il successivo un intervallo di tempo simulato mediante il metodo  sleep()
 *
 * L’indirizzo IP di dategroup viene introdotto  da linea di comando
 * Si è scelto di utilizzare un indirizzo di default in caso di errori con quello specificato
 *
 * @author Alberto Martino
 * @version 1.0
 */
public class MainClassServer {
    /**
     * indirizzo del gruppo di multicast
     */
    private final InetAddress multicastGroup;
    /**
     * porta associata all'indirizzo multicast
     */
    private final int port;
    /**
     * durata intervallo di attesa prima del prossimo invio
     */
    final static int DEFAULT_WAIT_TIME = 1000;

    /**
     *
     * Si tenta di assegnare dei valori per inizializzare un client, quindi avvio quel client;
     * Se l'input non è valido, si tenta di assegnare valori di default, notificandolo
     * Se anche con i valori di default, l'operazione fallisce, si notifica stampando il trace
     */
    public static void main(String[] args){
        try {
            var inputData = getInputs(args, Dategroup.DEFAULT_ADDRESS, Dategroup.DEFAULT_PORT);
            new MainClassServer(inputData.getAddress(), inputData.getPort()).start();
        }
        catch (UnknownHostException e) { System.err.println("unexpected error"); e.printStackTrace(); }
    }

    /**
     * @param address indirizzo del gruppo di multicast
     * @param port porta a cui associare il socket di multicast
     */
    public MainClassServer(InetAddress address, int port) {
        this.multicastGroup = address;
        this.port = port;
    }

    /**
     * invia sul gruppo di multicast la data e l’ora ad intervalli regolari
     */
    public void start() {
        try (var datagramSocket = new DatagramSocket()) {
            while (true) {
                try {
                    var dateTimeText = Dategroup.convert(LocalDateTime.now());
                    var datagramPacket = new DatagramPacket(
                            dateTimeText.getBytes(),
                            dateTimeText.length(),
                            this.multicastGroup,
                            this.port
                    );
                    datagramSocket.send(datagramPacket);
                    Thread.sleep(DEFAULT_WAIT_TIME);
                }
                catch (DateTimeException dateTimeException) { System.err.println("Errore di conversione della data"); }
            }
        } catch (IOException | InterruptedException exception) { exception.printStackTrace(); }
    }
}
