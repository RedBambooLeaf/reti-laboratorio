package com.albertomartino.assignements.assignement9;

/*
Struttura di convenienza per elaborare l'RTT nel sistema.
Chi utilizza questa classe deve solo preoccuparsi di aggiungere i sample che desidera e, in qualsiasi momento,
ottenere le stime (min, max e RTT).
 */
public class RTT {
    private final float alpha;
    private final float alphaComplement;
    private long minSampleRTT, maxSampleRTT;
    private float estimatedRTT;

    public RTT(float alpha) {
        this.alpha = alpha;
        alphaComplement = 1 - alpha;
        minSampleRTT = Long.MAX_VALUE;
        maxSampleRTT = Long.MIN_VALUE;
    }

    public float getAvg() { return estimatedRTT; }
    public long getMin() { return minSampleRTT; }
    public long getMax() { return maxSampleRTT; }

    public void Update(long sampleRTT) {
        estimatedRTT = alphaComplement * estimatedRTT + sampleRTT * alpha;
        if (sampleRTT < minSampleRTT) {
            minSampleRTT = sampleRTT;
        }
        if (sampleRTT > maxSampleRTT) {
            maxSampleRTT = sampleRTT;
        }
    }
}
