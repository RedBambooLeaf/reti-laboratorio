package com.albertomartino.assignements.assignement9;
import java.io.IOException;
import java.net.*;

public class Client {
    private static final float ALPHA = 0.125f;  // [0, 1] determina la gravità di un sample nel calcolo del RTT
    private static final int TIMEOUT = 2000;    // Tempo concesso per la receive
    private static final int NUMBER_OF_MESSAGES = 10;
    private final RTT rtt;  // classe utile all'istanza di client per elaborare RTT (inclusi massimo e minimo)
    private final DatagramSocket dgSocket;
    private final DatagramPacket dgPkgOut; // pacchetto in uscita
    private int numberOfLost;   // numero di messaggi persi

    public static void main(String[] args) {
        int port = -1;
        String serverName = null;

        // Valido gli argomenti secondo la convenzione richiesta
        var isFirstArgumentWrong = true;
        if (args != null) {
            if (args.length == 2) {
                serverName = args[0];
                isFirstArgumentWrong = false;
                try { port = Integer.parseInt(args[1]); }
                catch (Exception exception) { port = -1; }
            }
        }
        var areInputsValid = port != -1 && serverName != null;
        if (!areInputsValid) {
            System.out.println("ERR - arg " + (isFirstArgumentWrong ? "1" : "2"));
            return;
        }

        // Preparo le risorse necessarie ad avviare un client UDP, se riscontro un errore termino
        try (var datagramSocket = new DatagramSocket()) {
            var inetAddress = InetAddress.getByName(serverName);
            if (!inetAddress.isReachable(1000)) { throw new Exception("Unreachable Host"); }
            var dgPkgOut = new DatagramPacket(new byte[1], 0, inetAddress, port);
            datagramSocket.setSoTimeout(TIMEOUT);
            try { new Client(datagramSocket, dgPkgOut).Start(); }
            catch (IOException e) { System.out.println("IO error when sending/receiving from server"); e.printStackTrace(); }
        }
        catch (UnknownHostException exception) { System.out.println(serverName + " is an unknown host name"); }
        catch (IllegalArgumentException exception) { System.out.println("port " + port + " is out of reach"); }
        catch (SecurityException exception) { System.out.println("a security manager doesn't allow this client to launch properly"); }
        catch (SocketException exception) { System.out.println("the socket could not be opened, or the socket could not be bound.");  }
        catch (Exception exception) { exception.printStackTrace(); System.out.println("Unexpected Error"); }
    }

    private Client(DatagramSocket datagramSocket, DatagramPacket dgPkgOut) {
        this.rtt = new RTT(ALPHA);
        this.dgSocket = datagramSocket;
        this.dgPkgOut = dgPkgOut;
    }

    public void Start() throws IOException {
        // Preparo un generico pacchetto per ricevere dati
        var dgPkgIn = new DatagramPacket(new byte[DefaultValues.BUFFER_SIZE], DefaultValues.BUFFER_SIZE);
        var request = new RequestMessage();
        for (int messageNumber = 0; messageNumber < NUMBER_OF_MESSAGES; messageNumber++) {
            // Preparo il messaggio da inviare
            request.Update(messageNumber);

            // Preparo il pacchetto e lo invio
            dgPkgOut.setData(request.bytes, 0, request.length);
            dgSocket.send(dgPkgOut);

            // Se il pacchetto non arriva entro TIMEOUT ms lo considero perso
            var wasLost = false;
            try { dgSocket.receive(dgPkgIn); }
            catch (SocketTimeoutException timeoutException) { wasLost = true; numberOfLost++; }

            // Stampo un messaggio relativo all'ultimo scambio ed eventualmente aggiorno lo stato di RTT con il nuovo sample registrato
            System.out.printf("%s RTT: ", request.text);
            if (!wasLost) {
                var sample = System.currentTimeMillis() - request.recordTime;
                rtt.Update(sample);
                System.out.println(sample + " ms");
            }
            else {
                System.out.println("*");
            }
        }
        PrintStats();
    }

    private void PrintStats() {
        System.out.printf(" ---- PING Statistics ----\n" +
                        "%d packets transmitted, %d packets received, %.2f%% packet loss\n" +
                        "round-trip (ms) min/avg/max = %d//%.2f//%d",
                NUMBER_OF_MESSAGES, NUMBER_OF_MESSAGES - numberOfLost, numberOfLost / (float) NUMBER_OF_MESSAGES,
                rtt.getMin(), rtt.getAvg(), rtt.getMax());
    }

    // Una struttura di convenienza per aggiornare un messaggio di richiesta (secondo la convenzione stabilita) dato il suo numero progressivo
    private static class RequestMessage {
        public String text;
        public byte[] bytes;
        public int length;
        public long recordTime;

        public void Update(int progressiveNumber) {
            recordTime = System.currentTimeMillis();
            text = String.format("PING %d %d", progressiveNumber, recordTime);
            bytes = text.getBytes();
            length = bytes.length;
        }
    }
}