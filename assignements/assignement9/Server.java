package com.albertomartino.assignements.assignement9;
import com.albertomartino.utilities.Random;
import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Set;

public class Server {
    private final Selector selector; // Il selector che l'istanza di server utilizza per ottenere le chiavi

    public static void main(String[] args) {
        // Valido gli argomenti secondo la convenzione richiesta
        int port = -1;
        try { port = Integer.parseInt(args[0]); }
        catch (Exception exception) { /*nop*/ }
        var areInputsValid = args != null && args.length == 1 && port != -1;
        if (!areInputsValid) { System.out.println("ERR - arg 1"); return; }

        // Preparo le risorse necessarie ad avviare un thread server UDP con selector, se riscontro un errore termino
        try (
                var datagramChannel = DatagramChannel.open();
                var selector = Selector.open()
        ) {
            var serverAddress = new InetSocketAddress(port);
            datagramChannel.bind(serverAddress);
            datagramChannel.configureBlocking(false);
            var clientKey = datagramChannel.register(selector, SelectionKey.OP_READ);
            clientKey.attach(new Attachment());
            try { new Server(selector).Start(); }
            catch (IOException exception) { System.out.println("IO Error selecting keys"); }
        }
        catch (IllegalArgumentException exception) { System.out.println(port + " is outside the specified range of valid port values"); }
        catch (SecurityException exception) { System.out.println("a security manager doesn't allow to retrieve the address by it's name"); }
        catch (IOException exception) { System.out.println("IO error while opening connection"); }
        catch (Exception exception) { exception.printStackTrace(); System.out.println("Unexpected Error"); }
    }

    private Server(Selector selector) { this.selector = selector; }

    public void Start() throws IOException {
        while(true){
            // Seleziono chiavi, appena ne ho di nuove disponibili
            var keys = RefreshKeys();
            if (keys == null) continue;

            // Se una chiave non è valida oppure è leggibile/scrivibile ma non riesco a leggere/scrivere, proseguo con la successiva
            for (var key : keys)
            {
                if (!key.isValid()) { continue; }
                if (key.isReadable() && !TryRead(key)) { continue; }
                if (key.isWritable() && !TryWrite(key)) { continue; }
            }
        }
    }

    // Rimuovo le chiavi precedenti, ritorno le nuove chiavi disponibili oppure null se nessuna nuova chiave è disponibile
    private Set<SelectionKey> RefreshKeys() throws IOException {
        selector.selectedKeys().clear();
        if (selector.select() == 0) { return null; }
        return selector.selectedKeys();
    }

    // Provo a leggere dal canale e registrare l'interesse a scrivere con opportuno ritardo oppure scartare il messaggio di richiesta
    // Stampo il messaggio di log del server discriminando lo scarto dalla ricezione avvenuta con successo
    // Restituisco true sse l'operazione, qualsiasi sia stata la scelta, è andata a buon fine
    private boolean TryRead(SelectionKey key) {
        var channel = (DatagramChannel) key.channel();
        try {
            var attachment = Attachment.ReceiveAndUpdate(key, channel);
            PrintServerLogMessageCommonPrefix(attachment);
            var isDiscardingMessage = Random.Between(0f, 1f) <= 0.25f;
            if (isDiscardingMessage) { System.out.println("not sent");}
            else {
                var delay = Random.Between(100, 300);
                System.out.println("delayed " + delay + " ms");
                DelayedInterestOps(key, SelectionKey.OP_WRITE, delay);  // Qui viene lanciato un thread
            }
            return true;
        }
        catch(IOException e) {
            CloseAndUnregister(key, channel, "reading");
            return false;
        }
    }

    // Provo a scrivere sul canale e registrare l'interesse a leggere, restituisco true sse l'operazione va a buon fine
    private boolean TryWrite(SelectionKey key) {
        var channel= (DatagramChannel) key.channel();
        try {
            var con = (Attachment) key.attachment();
            channel.send(con.msg, con.soAddress);
            con.msg.clear();
            key.interestOps(SelectionKey.OP_READ);
        } catch(IOException ioException){
            CloseAndUnregister(key, channel, "writing");
            return false;
        }
        return true;
    }

    // Tento di chiudere la connessione con client e deregistrare la chiave
    private static void CloseAndUnregister(SelectionKey key, Closeable client, String state) {
        try { client.close(); }
        catch (IOException ioException) { ioException.printStackTrace(); }
        System.out.println("IOError error while " + state);
        key.cancel();
    }

    // Disiscrive le chiave per sostiuirne gli interessi con quelli specificati dopo un certo ritardo risvegliando il selettore
    private void DelayedInterestOps(SelectionKey key, int flags, int delay) {
        key.interestOps(0);
        new Thread(() -> {
            try {
                Thread.sleep(delay);
                key.interestOps(flags);
            }
            catch (Exception e) { e.printStackTrace(); }
            selector.wakeup();
        }).start();
    }

    // Stampa il prefisso del log del server che ha stesso formato indipendentemente dalla scelta compiuta di scartare o meno un pacchetto
    private void PrintServerLogMessageCommonPrefix(Attachment attachment) {
        var msg = new String(attachment.msg.array(), 0, attachment.msg.limit());
        var saSocketInet = (InetSocketAddress) attachment.soAddress;
        var port = saSocketInet.getPort();
        var saInetAddr = saSocketInet.getAddress();
        var addr = saInetAddr.getHostAddress();
        System.out.printf("%s:%d> %s ACTION: ", addr, port, msg);
    }

    // Una struttura di convenienza per gestire l'attachment di una chiave osservata
    private static class Attachment {
        public ByteBuffer msg;
        public SocketAddress soAddress;

        public Attachment() {
            msg = ByteBuffer.allocate(DefaultValues.BUFFER_SIZE);
        }

        public static Attachment ReceiveAndUpdate(SelectionKey key, DatagramChannel channel) throws IOException {
            var attachment = (Attachment) key.attachment();
            attachment.msg.clear();
            attachment.soAddress = channel.receive(attachment.msg);
            attachment.msg.flip();
            return attachment;
        }
    }
}
