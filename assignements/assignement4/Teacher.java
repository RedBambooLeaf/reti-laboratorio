package com.albertomartino.assignements.assignement4;

public class Teacher extends User {
    public Teacher(Tutor tutor) {super(tutor);}

    @Override
    protected Computer[] GetComputers() throws Exception {
        return tutor.AcquireTeacherComputers();
    }
}
