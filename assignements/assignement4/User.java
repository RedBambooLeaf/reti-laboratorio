package com.albertomartino.assignements.assignement4;
import com.albertomartino.utilities.Random;
import com.albertomartino.utilities.Range;
import com.albertomartino.utilities.Threads;

// tutti gli utenti estendono questa classe
public abstract class User implements Runnable {
    // riferimento al tutor e parametri dell'utente (tempo di permanenza, numero di accessi, intervallo accesso)
    protected final Tutor tutor;
    private static final Range<Integer> numberOfAccessesRange= new Range<>(1, 3);
    private static final Range<Long> permanenceSleepRange = new Range<>(1000L, 2000L); // in millis
    private static final Range<Long> accessWaitSleepRange = new Range<>(0L, 20000L); // in millis

    public User(Tutor tutor) {this.tutor = tutor;}

    @Override
    public void run() {
        // determino quale è il numero di accessi da effettuare
        var numberOfAccesses = Random.Between(numberOfAccessesRange.min, numberOfAccessesRange.max);

        // effettuo gli accessi richiesti
        for (int i = 0; i < numberOfAccesses; i++) {
            try {
                UseComputer();
            }
            catch (Exception e) {   // se qualcosa va storto, termino
                break;
            }
        }
    }
    private void UseComputer() throws Exception {

        // simulo l'attesa fra un tentativo di accesso e un altro
        Threads.RandomSleep(accessWaitSleepRange.min, accessWaitSleepRange.max);

        Computer[] computers = null;
        try {
            // ottengo il/i computer; ogni specializzazione di User specializzerà il suo get
            computers = GetComputers();
        }
        catch (InterruptedException exception) {    // non è stato possibile ottenere un computer
            System.out.printf("User %d rejected\n", Thread.currentThread().getId());
            throw exception;
        }

        try {
            // utilizzo il/i computer
            UseComputers(computers);
        }
        catch (InterruptedException exception) {
            System.out.printf("User %d interrupted\n", Thread.currentThread().getId());
            throw exception;
        }
        finally {
            // rilascio il/i computer anche in caso venga interrotto
            tutor.Release(computers);
        }

    }

    // Ho scelto di modellare l'unità ricevuta come un array di computer così da generalizzare docente vs altri utenti
    protected abstract Computer[] GetComputers() throws Exception;

    // ogni specializzazione di User può fare override per utilizzare i/il pc come desidera
    protected void UseComputers(Computer[] computers) throws InterruptedException {
        // simulo un certo tempo di permanenza al computer
        Threads.RandomSleep(permanenceSleepRange.min, permanenceSleepRange.max);
    }
}
