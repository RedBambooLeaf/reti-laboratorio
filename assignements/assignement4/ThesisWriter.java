package com.albertomartino.assignements.assignement4;

public class ThesisWriter extends User {
    private final int thesisWriterComputerIndex;

    public ThesisWriter(Tutor tutor, int thesisWriterComputerIndex) {
        super(tutor);
        this.thesisWriterComputerIndex = thesisWriterComputerIndex;
    }

    @Override
    protected Computer[] GetComputers() throws Exception {
        return tutor.AcquireThesisWriterComputer(thesisWriterComputerIndex);
    }
}
