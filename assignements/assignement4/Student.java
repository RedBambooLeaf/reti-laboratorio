package com.albertomartino.assignements.assignement4;

public class Student extends User {
    public Student(Tutor tutor) {
        super(tutor);
    }

    @Override
    protected Computer[] GetComputers() throws Exception {
        return tutor.AcquireStudentComputer();
    }
}
