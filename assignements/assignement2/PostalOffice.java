package com.albertomartino.assignements.assignement2;
import java.util.*;
import java.util.concurrent.*;

// si occupa di gestire il flusso di clienti
public class PostalOffice implements Runnable {
    private final ExecutorService deskPool;
    private final BlockingQueue<Customer> firstWaitingQueue;
    private Customer[] allCustomers;
    private int numberOfCustomers;
    private final int numberOfDesks;

    public PostalOffice(int numberOfDesks, int extraCapacity) {
        this.numberOfDesks = numberOfDesks;
        numberOfCustomers = 0;
        firstWaitingQueue = new LinkedBlockingQueue<>();
        deskPool = new ThreadPoolExecutor(
                numberOfDesks,
                numberOfDesks,
                0L,
                TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<>(extraCapacity));
    }

    public void AcceptNewCustomer() {
        // Creo un cliente assegnandoli l'id e lo aggiungo alla prima coda
        var customer = new Customer(numberOfCustomers++);
        firstWaitingQueue.add(customer);
    }

    @Override
    public void run(){
        // se si dovesse sollevare un'eccezione d'interruzione chiudo l'ufficio rigettando i clienti
        try {
            OpenPostalOffice();
        }
        catch (InterruptedException ex1) {
            ClosePostalOffice();
        }
    }

    // ritorna statistiche sulla gestione del flusso
    public PostalOfficeStats getStats() throws IllegalArgumentException {
        try {
            return new PostalOfficeStats(allCustomers, numberOfDesks);
        }
        catch (Exception ex) {
            return null;
        }
    }

    private void OpenPostalOffice() throws InterruptedException {

        // per le ipotesi fatte, all'apertura so che tutti i clienti sono nella prima coda
        // li registro così che poi possa eventualmente elaborare delle statistiche più tardi
        allCustomers = new Customer[firstWaitingQueue.size()];
        allCustomers = firstWaitingQueue.toArray(allCustomers);

        // struttura dati di supporto per rimuovere dalla prima coda gli utenti passati alla seconda
        var toRemoveFromFirstRoom = new ArrayList<Customer>();

        // finché ci sono ancora clienti non serviti,
        // tento di far scorrere la prima coda verso la seconda
        var customerServed = 0;
        while (customerServed < allCustomers.length) {
            if (Thread.interrupted()) { throw new InterruptedException(); }

            // tento di aggiungere clienti alla seconda coda
            for (var customer : firstWaitingQueue) {
                try {
                    deskPool.execute(customer);
                    toRemoveFromFirstRoom.add(customer);
                }
                catch (RejectedExecutionException ex) {
                    break;
                }
            }

            // tolgo dalla prima coda gli eventuali clienti che si sono spostati nella seconda
            if (toRemoveFromFirstRoom.size() > 0) {
                customerServed += toRemoveFromFirstRoom.size();
                firstWaitingQueue.removeAll(toRemoveFromFirstRoom);
                toRemoveFromFirstRoom.clear();
            }
        }

        deskPool.shutdown();
        while (!deskPool.isTerminated())
            deskPool.awaitTermination(1000L, TimeUnit.MILLISECONDS);
    }

    private void ClosePostalOffice() {
        // rigetto tutti i clienti nella prima coda
        for (var customer : firstWaitingQueue) {
            customer.Dismiss();
        }
        firstWaitingQueue.clear();

        // rigetto tutti i clienti nella seconda coda e agli sportelli
        for (var waitingCustomerTask : deskPool.shutdownNow()) {
            ((Customer)waitingCustomerTask).Dismiss();
        }
    }
}
