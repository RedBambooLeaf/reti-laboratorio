package com.albertomartino.assignements.assignement2;
import com.albertomartino.utilities.Threads;

public class Customer implements Runnable {
    private static final long maxWaitInMillis = 5000;
    private static final long minWaitInMillis = 1;
    private int id;
    private long deskID;
    private boolean isDismissed;
    private long arrivedAt, startedAt, finishedAt;

    public boolean IsDismissed() {return isDismissed;}
    public double getServeTime() {return (finishedAt - startedAt)/1000.0;}
    public double getWaitTime() {return (startedAt - arrivedAt)/1000.0;}
    public long getDeskID() {return deskID;}

    public Customer(int id) {
        this.id = id;
        arrivedAt = System.currentTimeMillis();
    }

    public void  Dismiss() {
        isDismissed = true;
    }

    @Override
    public void run() {
        startedAt = System.currentTimeMillis();

        try {
            // avvia una sleep di durata casuale fra minWaitInMillise e maxWaitInMillis
            Threads.RandomSleep(minWaitInMillis, maxWaitInMillis);
        }
        catch (InterruptedException ex) {
            Dismiss();
        }

        finishedAt = System.currentTimeMillis();
        deskID = Thread.currentThread().getId();
        System.out.printf("Desk_%d) Dispatched customer N_%d \r", deskID, id);
    }
}
