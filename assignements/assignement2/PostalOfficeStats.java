package com.albertomartino.assignements.assignement2;
import java.util.HashMap;
import java.util.Map;

public class PostalOfficeStats {
    public int total;
    public int served;
    public int dismissed;
    public double avgServingTime, avgWaitTime;
    public double minServingTime, minWaitTime;
    public double maxServingTime, maxWaitTime;
    public Map<Long, Integer> deskLoad;

    public PostalOfficeStats(Customer[] customers, int numberOfDesks) {

        if (customers == null || numberOfDesks <= 0) {
            throw new IllegalArgumentException();
        }

        total = customers.length;
        served = dismissed = 0;
        minServingTime = minWaitTime = Double.MAX_VALUE;
        maxServingTime = maxWaitTime = Double.MIN_VALUE;
        deskLoad = new HashMap<>(numberOfDesks);

        var sumServingTime = 0.0;
        var sumWaitingTime = 0.0;
        for (var customer : customers) {
            if (customer.IsDismissed()) {
                dismissed++;
            }
            else {
                served++;

                var serveTime = customer.getServeTime();
                if (serveTime < minServingTime) {
                    minServingTime = serveTime;
                }
                if (serveTime > maxServingTime) {
                    maxServingTime = serveTime;
                }
                sumServingTime += serveTime;

                var waitTime = customer.getWaitTime();
                if (waitTime < minWaitTime) {
                    minWaitTime = waitTime;
                }
                if (waitTime > maxWaitTime) {
                    maxWaitTime = waitTime;
                }
                sumWaitingTime += waitTime;

                var deskID = customer.getDeskID();
                var deskLoadCount = deskLoad.getOrDefault(deskID, 0);
                deskLoad.put(deskID, ++deskLoadCount);
            }
        }

        if (served == 0) {
            minServingTime = maxServingTime = minWaitTime = maxWaitTime = avgWaitTime = avgServingTime = 0;
        }
        else {
            avgServingTime = sumServingTime / served;
            avgWaitTime = sumWaitingTime / served;
        }
    }
}
