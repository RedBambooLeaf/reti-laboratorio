package com.albertomartino.assignements.assignement2;

/*
Assunzioni:
 - i clienti da accettare sono numberOfCustomers < Integer.MAX_VALUE
 - tutti i clienti entrano prima che l'ufficio postale inizi ad accogliere nella II stanza
    ("si preveda di far entrare tutte le persone nell'ufficio postale, all'inizio del programma")
*/

public class MainClass {
    private static final int defaultNumberOfDesks = 4;
    private static final int defaultExtraCapacity = 12;     // k nel testo
    private static final int numberOfCustomers = 50;

    public static void main(String[] args) {
        // creo un nuovo ufficio postale
        var postalOffice = new PostalOffice(defaultNumberOfDesks, defaultExtraCapacity);

        // faccio entrare preventivamente numberOfCustomers clienti
        for (int i = 0; i < numberOfCustomers; i++) {
            postalOffice.AcceptNewCustomer();
        }

        // avvio l'attività dell'ufficio
        var postalOfficeThread = new Thread(postalOffice);
        postalOfficeThread.start();

        // attendo che l'ufficio serva tutti i clienti
        try {
            postalOfficeThread.join();
        }
        catch (InterruptedException ex1) {
            // se vengo interrotto, interrompo l'attività dell'ufficio postale
            // al quale concedo ulteriori 3 secondi per chiudere
            postalOfficeThread.interrupt();
            try {
                postalOfficeThread.join(3000);
            }
            catch (InterruptedException ex2) {
                ex2.printStackTrace();
                return;
            }
        }

        // Se l'ufficio è riuscito a chiudere senza problemi stampo delle statistiche riguardanti la gestione dei flussi
        try {
            PrintPostalOfficeStats(postalOffice.getStats());
        }
        catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }

    private static void PrintPostalOfficeStats(PostalOfficeStats stats) {
        if (stats == null) {
            System.out.println("No stats available.");
            return;
        }

        System.out.printf(
                "** POSTAL OFFICE STATISTICS **\n\n" +
                "Total Customers:\t%d\n" +          // il numero di clienti totali
                "---------served:\t%d\n" +          // il numero di clienti serviti
                "------dismissed:\t%d\n\n" +        // il numero di clienti non serviti
                "---Serving Time:\t%f\t(avg)\n" +   // esprime il tempo medio di un cliente ad uno sportello
                "------------min:\t%f\n" +
                "------------max:\t%f\n\n" +
                "---Waiting Time:\t%f\t(avg)\n" +   // esprime il tempo medio d'attesa di un cliente
                "------------min:\t%f\n" +
                "------------max:\t%f\n\n",
                stats.total, stats.served, stats.dismissed,
                stats.avgServingTime, stats.minServingTime, stats.maxServingTime,
                stats.avgWaitTime, stats.minWaitTime, stats.maxWaitTime
        );

        // per ogni desk (identificato dal corrispondente id thread) indico il numero di clienti serviti
        System.out.print("Number of Customers per Desk\n<desk_id, #customers>\n");
        for (var key : stats.deskLoad.keySet()) {
            System.out.printf("<%d, %d>\n", key, stats.deskLoad.get(key));
        }
    }

}
