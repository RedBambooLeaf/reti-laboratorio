package com.albertomartino.assignements.assignement5;
import java.io.File;

public class Consumer implements Runnable {
    private final DirectoriesList directories;

    public Consumer(DirectoriesList directories) {
        this.directories = directories;
    }

    @Override
    public void run() {
        // Finché ci sono cartelle da esplorare, aspetto di poterne ottenere una (il suo full name)
        // quindi stampo il nome di ogni file contenuto in quella directory che sia, effettivamente un file leggibile
        // So che le cartelle da esplorare sono finite quando GetFileFullName restituisce null
        while (true) {
            try {
                var files = new File(directories.GetFileFullName()).listFiles(File::isFile);
                if (files != null) {
                    for (var file : files) {
                        System.out.println(file.getAbsolutePath());
                    }
                }
            }
            catch (NullPointerException nullPointerException) {
                break;
            }
            catch (InterruptedException iException) {
                iException.printStackTrace();
                return;
            }
            catch (SecurityException exception) {
                // discarded for security reasons
            }
        }
    }
}
