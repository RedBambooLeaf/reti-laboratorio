package com.albertomartino.assignements.assignement5;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;

public class Producer implements Runnable {
    private final DirectoriesList directories;
    private final File rootDirectory;

    public Producer(File rootDirectory, DirectoriesList directories) {
        this.directories = directories;
        this.rootDirectory = rootDirectory;
    }

    @Override
    public void run() {
        // Aggiungo il root alla lista di cartelle da esplorare e avvio la ricerca ricorsiva
        directories.AddDirectoryName(rootDirectory.getAbsolutePath());
        FindDirectoriesWithIterator(rootDirectory);
    }

    // le cartelle sono esplorate e aggiunte via iteratore (directory stream) sulla directory argomento
    private void FindDirectoriesWithIterator(File directory) {
        try (var directoryStream = Files.newDirectoryStream(directory.toPath())) {
            var iterator = directoryStream.iterator();
            while (iterator.hasNext()) {
                var file = iterator.next().toFile();
                if (file.isDirectory()) {
                    directories.AddDirectoryName(file.getAbsolutePath());
                    FindDirectoriesWithIterator(file);
                }
            }
        }
        catch (AccessDeniedException|SecurityException securityException) {
            System.out.printf("Couldn't crawl %s because of security reasons\n", directory.getAbsolutePath());
        }
        catch (IOException ioException) {
            ioException.printStackTrace();
            return;
        }


        if (directory == rootDirectory) {
            directories.SetIsDirectoryExplored();
        }
    }

    // le cartelle sono esplorate e aggiunte dal listFiles con filtro sulla directory argomento
    private void FindDirectoriesWithListFiles(File directory) {
        try {
            var subDirectories = directory.listFiles(File::isDirectory);
            if (subDirectories != null) {
                for (var file : subDirectories) {
                    directories.AddDirectoryName(file.getAbsolutePath());
                    FindDirectoriesWithListFiles(file);
                }
            }
        }
        catch (SecurityException exception) {
            System.out.printf("Couldn't crawl %s because of security reasons\n", directory.getAbsolutePath());
        }

        if (directory == rootDirectory) {
            directories.SetIsDirectoryExplored();
        }
    }
}
