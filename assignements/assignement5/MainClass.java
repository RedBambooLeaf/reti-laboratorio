package com.albertomartino.assignements.assignement5;
import java.io.File;
import java.nio.file.InvalidPathException;
import java.util.Scanner;

public class MainClass {
    private static final int numberOfConsumers = 10;

    public static void main(String[] args) {
        // Stampo un messaggio di benvenuto indicando, se possibile, work path e home path
        PrintWelcomeMessage();

        // try with resources per leggere il path da input
        try (var scanner = new Scanner(System.in)) {
            // la radice sarà la root directory specificata
            var root = new File(scanner.nextLine());
            if (!root.isDirectory()) { throw new InvalidPathException(root.getAbsolutePath(),  "not a valid directory"); }

            // istanzio la classe responsabile di gestire la coda di cartelle da esplorare
            var directories = new DirectoriesList();

            // avvio il thread produttore e i thread consumatori
            new Thread(new Producer(root, directories)).start();
            for (int i = 0; i < numberOfConsumers; i++) {new Thread(new Consumer(directories)).start();}
        }
        // se qualcosa è andato storto, lo comunico
        catch (Exception exception) {
            System.out.println("Folder crawling completed with errors");
            exception.printStackTrace();
        }
    }

    private static void PrintWelcomeMessage() {
        String welcomeMessage;
        try {
            welcomeMessage = String.format("Work path: %s\nHome path: %s\n",
                    new File(".").getAbsolutePath(),
                    new File(System.getProperty("user.home")).getAbsolutePath());
        }
        catch (Exception e) {
            welcomeMessage = "";
        }

        welcomeMessage += "Insert the path to the root directory you want to crawl: ";
        System.out.print(welcomeMessage);
    }
}
