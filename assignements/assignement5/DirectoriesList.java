package com.albertomartino.assignements.assignement5;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

// Implementa la coda delle cartelle da esplorare gestendone la sincronizzazione
// Utilizzando una LinkedList e un booleano atomico ad indicare che non ci sono più cartelle da esplorare
public class DirectoriesList {
    private final LinkedList<String> directories;
    private final AtomicBoolean isDirectoryExplored;

    public DirectoriesList() {
        directories = new LinkedList<>();
        isDirectoryExplored = new AtomicBoolean();
    }

    // Indico che non ci sono più cartelle da esplorare e risveglio tutti i thread eventualmente in attesa
    public void SetIsDirectoryExplored() {
        isDirectoryExplored.set(true);
        NotifyAll();
    }
    public synchronized void NotifyAll() {notifyAll();}

    public synchronized String GetFileFullName() throws InterruptedException {
        String fullName;

        // Finché non recupero una directory, attendo se ce ne saranno ancora, termino altrimenti
        while ((fullName = directories.poll()) == null) {
            if (isDirectoryExplored.getAcquire()) {
                return null;
            }
            else {
                this.wait();
            }
        }


        return fullName;
    }

    // Aggiungo una directory alla lista e risveglio eventuali thread in attesa
    public synchronized void AddDirectoryName(String name) {
        directories.add(name);
        this.notify();
    }
}
