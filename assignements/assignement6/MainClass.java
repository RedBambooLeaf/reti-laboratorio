package com.albertomartino.assignements.assignement6;
import com.albertomartino.utilities.Range;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerArray;

// Nell'assignement sono gestite solamente le caratteristiche esplicitate nella consegna
// Una qualsiasi eccezione porta alla stampa dello stack trace e di un messaggio di errore generico, liberando eventuali risorse auto-closable
public class MainClass {
    // Utilizzo un array di interi che consente operazioni di modifica atomiche ai suoi valori per memorizzare l'occorrenza delle causali
    private static final AtomicIntegerArray causalsCount = new AtomicIntegerArray(BankAccountRecordCausal.getCardinality());
    // Gli intervalli dentro ai quali voglio che cadano il numero di account creati e il numero di record creati per account
    private static final Range<Integer> numberOfAccountsRange = new Range<>(2, 2);
    private static final Range<Integer> numberOfRecordsPerAccountRange = new Range<>(5, 5);

    public static void main(String[] args) {
        // Utilizzo uno scanner per prendere il path da input
        try (var scanner = new Scanner(System.in)) {
            // Stampo un messaggio di benvenuto indicando, se possibile, work path e home path
            PrintWelcomeMessage();

            // Creo il path
            Path path = Paths.get(scanner.nextLine());

            // Creo gli account
            BankAccountsExampleCreator.CreateAccounts(numberOfAccountsRange, numberOfRecordsPerAccountRange, path);

            // Avvio il thread dispatcher che si occuperà di smistare gli account trovati ad altri thread via thread pool fornito
            var executor = Executors.newCachedThreadPool();
            var dispatcher = new Thread(new BankAccountRecordDispatcher(path, executor));
            dispatcher.start();

            // Attendo che tutti i thread abbiano finito (non ordino uno shutdown al thread pool prima che il dispatcher abbia terminato)
            dispatcher.join();
            executor.shutdown();
            while (!executor.isTerminated()) {executor.awaitTermination(1, TimeUnit.SECONDS);}

            // Stampo i risultati ottenuti, incluso un conteggio delle causali in totale
            var totalCausalsRecorded = 0;
            for (int i = 0; i < causalsCount.length(); i++) {
                totalCausalsRecorded += causalsCount.get(i);
                System.out.printf("%s: %d\n", BankAccountRecordCausal.getCausal(i), causalsCount.get(i));
            }
            System.out.printf("Total: %d\n", totalCausalsRecorded);
        }
        catch (Exception e) {
            System.out.println("Completed with errors");
            e.printStackTrace();
        }
    }

    // Questo metodo sarà chiamato dai thread che analizzano account per contare le causali
    public static void IncrementCausalCount(BankAccountRecordCausal causal) { causalsCount.incrementAndGet(causal.getValue()); }

    // Stampa un semplice messaggio di benvenuto e la richiesta del filepath
    private static void PrintWelcomeMessage() {
        String welcomeMessage;
        try {
            welcomeMessage = String.format("Work path: %s\nHome path: %s\n",
                    new File(".").getAbsolutePath(),
                    new File(System.getProperty("user.home")).getAbsolutePath());
        }
        catch (Exception e) {
            welcomeMessage = "";
        }

        welcomeMessage += "Insert the filepath: ";
        System.out.print(welcomeMessage);
    }

}
