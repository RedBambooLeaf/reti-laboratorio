package com.albertomartino.assignements.assignement6;
import com.albertomartino.utilities.Random;
import java.util.HashMap;
import java.util.Map;

// Enumerazione delle causali con metodi e proprietà di istanza / statiche utili alla MainClass
// Non è gestita programmaticamente l'univocità dei valori assegnati
public enum BankAccountRecordCausal {
    Bonifico(0),
    Accredito(1),
    Bollettino(2),
    F24(3),
    PagoBancomat(4);

    // Memorizzo il numero di causali possibili per poterlo comunicare in seguito via getter statica
    private final static int cardinality = BankAccountRecordCausal.values().length;
    public static int getCardinality() {return cardinality;}

    // Ad ogni causale è associato un intero: fornisco metodi per ottenerlo data la causale
    private final int value;
    BankAccountRecordCausal(int value) { this.value = value;}
    public int getValue() {return value;}

    // Ad ogni intero è associata una causale: fornisco metodi per ottenerla dato l'intero
    private final static Map<Integer, BankAccountRecordCausal> map = new HashMap<>();
    static { for (var causal : BankAccountRecordCausal.values()) {map.put(causal.value, causal);} }
    public static BankAccountRecordCausal getCausal(int causalAsInt) throws IllegalArgumentException {
        var causal = map.get(causalAsInt);
        // Se l'intero passato non è stato mappato, allora lancio un'eccezione indicando l'errore
        if (causal == null) {
            throw new IllegalArgumentException("There's no entry in this class with such int value");
        }
        return causal;
    }

    // Per ottenere una causale casuale
    public static BankAccountRecordCausal getRandomCausal() { return getCausal(Random.Between(0, cardinality - 1)); } //TODO: utile gestione errori qui (?) chiedi
}