package com.albertomartino.assignements.assignement6;
import com.albertomartino.utilities.Random;
import com.albertomartino.utilities.Range;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.*;
import java.util.Date;

public class BankAccountsExampleCreator {

    // Creo un numero di account compreso nell'intervallo numberOfAccountsRange
    // Per ogni account creo un numero di record compreso nell'intervallo bankRecordsRangePerAccount
    // Ogni record ceato avrà assegnata una causale casuale
    public static void CreateAccounts(Range<Integer> numberOfAccountsRange, Range<Integer> bankRecordsRangePerAccount, Path path) throws Exception {
        // Preparo le date di ora e due anni fa, così da assegnarne casuali ai record che siano in questo intervallo
        long minDateInEpochSeconds = LocalDate.now().minusYears(2).atStartOfDay(ZoneId.systemDefault()).toInstant().getEpochSecond();
        long maxDateInEpochSeconds = LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant().getEpochSecond();

        // Tengo il conto dei record creati, scelgo il numero di account
        var totalRecords = 0;
        var numberOfAccounts = Random.Between(numberOfAccountsRange.min, numberOfAccountsRange.max);

        // Per la scrittura su file utilizzo un buffered writer, per ottenere la stringa json un object mapper
        try(BufferedWriter writer = Files.newBufferedWriter(path)){
            var objectMapper = new ObjectMapper();
            // Ad ogni iterazione creo un account con un nome canonico e un numero casuale di record quindi serializzo l'account
            for (int i = 0; i < numberOfAccounts; i++) {
                var account = new BankAccount(String.format("Account %d", i));
                var numberOfRecords = Random.Between(bankRecordsRangePerAccount.min, bankRecordsRangePerAccount.max);
                for (int j = 0; j < numberOfRecords; j++) {
                    var dateInSecond = Random.Between(minDateInEpochSeconds, maxDateInEpochSeconds);
                    account.addExampleRecord(BankAccountRecordCausal.getRandomCausal(), Date.from(Instant.ofEpochSecond(dateInSecond)));
                }
                totalRecords += numberOfRecords;
                writer.write(String.format("%s\n", objectMapper.writeValueAsString(account)));
            }
        }

        // Stampo il numero di account e record totali
        System.out.printf("Created %d account, %d records.\n", numberOfAccounts, totalRecords);
    }
}
