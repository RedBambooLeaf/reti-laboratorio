package com.albertomartino.assignements.assignement6;

public class BankAccountCausalCounter implements Runnable {
    private final BankAccount account;

    public BankAccountCausalCounter(BankAccount account) {this.account = account;}

    // Foreach sui record dell'account con conteggio delle relative causali via metodo statico della MainClass
    @Override
    public void run() { account.getRecords().stream().map(BankAccountRecord::getCausal).forEach(MainClass::IncrementCausalCount); }
}
