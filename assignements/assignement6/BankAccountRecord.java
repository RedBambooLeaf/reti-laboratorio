package com.albertomartino.assignements.assignement6;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import java.util.Date;

public class BankAccountRecord {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss") // Non Necessario, formatto la data nel .json
    private Date date;
    private BankAccountRecordCausal causal;

    public BankAccountRecordCausal getCausal() {return causal;}

    public BankAccountRecord(Date date, BankAccountRecordCausal causal) {
        this.date = date;
        this.causal = causal;
    }

    // Annotazioni Json superflue utilizzate per esplicitare il motivo per il quale sono stati aggiunti i seguenti metodi
    @JsonCreator
    public BankAccountRecord() {}
    @JsonGetter
    public Date getDate() {return date;}
}
