package com.albertomartino.assignements.assignement6;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import java.util.ArrayList;
import java.util.Date;

// Si suppone che l'holder sia univoco
public class BankAccount {
    private String holder;
    private ArrayList<BankAccountRecord> records;

    public ArrayList<BankAccountRecord> getRecords() {
        return records;
    }

    public BankAccount(String holder) {
        this.holder = holder;
        records = new ArrayList<>();
    }

    public void addExampleRecord(BankAccountRecordCausal causal, Date date) {
        var record = new BankAccountRecord(date, causal);
        records.add(record);
    }

    // Annotazioni Json superflue utilizzate per esplicitare il motivo per il quale sono stati aggiunti i seguenti metodi
    @JsonCreator
    public BankAccount() {}
    @JsonGetter
    public String getHolder() { return holder; }
}
