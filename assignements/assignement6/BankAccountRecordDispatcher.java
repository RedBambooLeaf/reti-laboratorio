package com.albertomartino.assignements.assignement6;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ExecutorService;

public class BankAccountRecordDispatcher implements Runnable {
    private final Path path;
    private final ExecutorService executor;
    private final ObjectMapper mapper = new ObjectMapper();

    public BankAccountRecordDispatcher(Path path, ExecutorService executor) {
        this.path = path;
        this.executor = executor;
    }

    @Override
    public void run() {
        try {
            // Chiamo il metodo ProcessLine per ogni linea del file
            Files.lines(path).forEach(this::ProcessLine);
        }
        catch (Exception e) {
            System.out.println("Error reading json file");
            e.printStackTrace();
        }
    }

    private void ProcessLine(String accountAsString) {
        try {
            // Lancio un thread per il conteggio sull'account che ho ottenuto deserializzando la linea
            executor.execute(new BankAccountCausalCounter(mapper.readValue(accountAsString, BankAccount.class)));
        }
        catch (IOException exception) {
            System.out.println("Invalid entry in json file");
            exception.printStackTrace();
        }
    }
}
