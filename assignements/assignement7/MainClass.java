package com.albertomartino.assignements.assignement7;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

/*
    Il programma risolve i filename passati via HTTP GET come indirizzi relativi al path specificato da linea di comando
 */
public class MainClass {
    private final static int DEFAULT_PORT = 5237;

    public static void main(String[] args) {

        // Stampo un messaggio di benvenuto richiedendo la root directory nella quale sono contenuti i file che si vogliono trasmettere
        PrintWelcomeMessage();

        // Tento validare e gestire il path specificato da linea di comando
        // Se il metodo ha successo HeaderManipulation avrà istanza e consentirà di tradurre da header GET a file path assoluti
        if (!TrySetRootDirectory()) {
            System.out.println("ROOT DIRECTORY: access failed");
            return;
        }

        // Tento di avviare il server
        var serverSocket = LaunchServer();
        if (serverSocket == null) {
            System.out.println("SERVER: launch failed");
            return;
        }

        // Il server passa eventuali socket client aperte ad appositi thread via threadpool
        // Se ci sono problemi interni, il server attende un certo tempo e riprova a mettersi in ascolto a meno che non venga interrotto
        // Se il server viene interrotto, aspetta che eventuali client handler abbiano finito il loro lavoro, a meno che non venga interrotto nuovamente
        var executor = Executors.newCachedThreadPool();
        System.out.println("SERVER: accepting clients...");
        while (true) {
            try {
                executor.execute(new ClientHandler(serverSocket.accept()));
            }
            catch (IOException ioException) {
                System.out.println("SERVER: accept IO Exception; will resume shortly...");
                try {
                    Thread.sleep(5000);
                }
                catch (InterruptedException interruptedException) {
                    System.out.println("SERVER: interrupted. Waiting for client handlers...");
                    break;
                }
            }
            catch (RejectedExecutionException rejectedExecutionException) {
                System.out.println("SERVER: cannot accept more requests; will resume in few minutes...");
                try {
                    Thread.sleep(60000);
                }
                catch (InterruptedException interruptedException) {
                    System.out.println("SERVER: interrupted. Waiting for client handlers...");
                    break;
                }
            }
        }

        // Ordino uno shutdown e attendo che i client handler abbiano terminato, a meno che non venga interrotto
        executor.shutdown();
        try {
            while (!executor.isShutdown()) { executor.awaitTermination(1, TimeUnit.SECONDS); }
        }
        catch (InterruptedException interruptedException1) {
            System.out.println("SERVER: interrupted, shut down.");
        }

        // Tento di chiudere la socket server
        try { serverSocket.close(); } catch (IOException ioException) { ioException.printStackTrace(); }
        System.out.println("SERVER: closed");
    }

    // Istanzio un server socket e faccio la bind al local host col numero di porta predefinito
    private static ServerSocket LaunchServer(){
        try {
            var serverSocket = new ServerSocket();
            serverSocket.bind(new InetSocketAddress(DEFAULT_PORT));
            try {
                System.out.printf("Server opened @\naddr:\t%s\nport:\t%d\n", InetAddress.getLocalHost().getHostAddress(), DEFAULT_PORT);
            }
            catch (UnknownHostException unknownHostException) {
                System.out.printf("Server opened on local host, port:\t%d\n", DEFAULT_PORT);
            }
            return serverSocket;
        }
        catch (IOException ioException) {
            ioException.printStackTrace();
            return null;
        }
    }

    // Istanzio un singoletto che si occuperà di tradurre da request line HTTP a filepath
    private static boolean TrySetRootDirectory() {
        try (var scanner = new Scanner(System.in)) {
            new HeaderManipulation(scanner.nextLine());
            return true;
        }
        catch (Exception exception) {
            exception.printStackTrace();
            return false;
        }
    }

    private static void PrintWelcomeMessage() {
        String welcomeMessage;
        try {
            welcomeMessage = String.format("Work path: %s\nHome path: %s\n",
                    new File(".").getAbsolutePath(),
                    new File(System.getProperty("user.home")).getAbsolutePath());
        }
        catch (Exception e) {
            welcomeMessage = "";
        }

        welcomeMessage += "Insert the path to the root directory where you store the files: ";
        System.out.print(welcomeMessage);
    }
}