package com.albertomartino.assignements.assignement7;
import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.NoSuchFileException;

// @TODO: avrei dovuto inviare i byte del file un po' alla volta, mano a mano letti dal file, e non tutti insieme
// @TODO: avrei dovuto evitare di utilizzare due stream differenti per la scrittura
// @TODO: avrei dovuto gestire meglio quando aprire e chiudere gli stream, magari con un try with resources
public class ClientHandler implements Runnable {
    private final Socket client;

    public ClientHandler(Socket client) { this.client = client; }

    @Override
    public void run() {
        var threadId = Thread.currentThread().getId();
        System.out.printf("HANDLER %d: connected to %s\n", threadId, client.getInetAddress().getHostAddress());

        // Tento di istanziare degli stream per la lettura/scrittura
        BufferedWriter writer;        // usato solo per l'header
        BufferedReader reader;
        DataOutputStream outToClient; // usato solo per il file inviato
        try {
            reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
            writer = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
            outToClient = new DataOutputStream((client.getOutputStream()));
        }
        catch (IOException ioException) {
            ioException.printStackTrace();
            return;
        }

        // Cerco di ottenere il file come array di byte e, a seconda dell'esito, preparo l'header da inviare
        String header;
        byte[] file = null;
        try {
            file = Files.readAllBytes(new File(HeaderManipulation.instance.GetAbsolutePath(reader.readLine())).toPath());
            header =  "HTTP/1.1 200 OK\n\r\n";
        }
        catch (IOException exception) {
            if (exception.getClass() == NoSuchFileException.class) {
                System.out.printf("HANDLER %d: invalid path %s\n", threadId, exception.getMessage());
                header = "HTTP/1.1 404 FILE NOT FOUND\n\r\n";
            }
            else  {
                System.out.printf("HANDLER %d: IO Exception loading file %s\n", threadId, exception.getMessage());
                header = "HTTP/1.1 503 SERVICE UNAVAILABLE\n\r\n";
            }
        }
        catch (InvalidPathException | SecurityException pathException) {
            System.out.printf("HANDLER %d: invalid path %s\n", threadId, pathException.getMessage());
            header = "HTTP/1.1 404 FILE NOT FOUND\n\r\n";
        }
        catch (OutOfMemoryError memoryError) {
            System.out.printf("HANDLER %d: internal error loading file %s\n", threadId, memoryError.getMessage());
            header = "HTTP/1.1 500 INTERNAL SERVER ERROR\n\r\n";
        }

        // Scrivo l'header
        try {
            writer.write(header);
            writer.flush();
        }
        // Se non riesco ad inviare l'header, non invierò neanche un file
        catch (IOException ioException) {
            file = null;
            System.out.printf("HANDLER %d: IO Exception writing header %s\n", threadId, ioException.getMessage());
        }

        // Se ho un file da inviare, lo invio
        if (file != null) {
            try {
                outToClient.write(file, 0, file.length);
                outToClient.flush();
            }
            catch (IOException ioException) {
                System.out.printf("HANDLER %d: IO Exception on %s\n", threadId, ioException.getMessage());
            }
        }

        // Tento di chiudere la socket client e gli stream
        try {
            writer.close();
            reader.close();
            outToClient.close();
            client.close();
        }
        catch (IOException ioException) {
            System.out.printf("HANDLER %d: IO Exception closing client socket or stream/s  %s\n", threadId, ioException.getMessage());
        }
    }
}
