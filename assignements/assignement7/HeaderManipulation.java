package com.albertomartino.assignements.assignement7;
import java.io.File;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class HeaderManipulation {
    public static HeaderManipulation instance; // HeaderManipulation è un singleton

    // Regex per esprimere una request line valida (solo GET HTTP)
    private static final String regex = "^GET\\s(\\/|\\\\).*\\sHTTP(\\/|\\\\)[0-9].[0-9]$";

    // Predicato relativo alla regex con il quale è possibile testare la validità di una request line
    private static final Predicate<String> evaluateGetPattern = Pattern.compile(regex).asMatchPredicate();

    // Metodo utile a conformare il path relativo ricevuto con i giusti separatori per l'OS su cui è in esecuzione l'host server
    private static String ConformWithFileSeparator(String source) {
        if (source.contains(new StringBuffer("\\"))) {
            source = source.replaceAll("\\\\*", File.separator);
        }
        if (source.contains(new StringBuffer("//*"))) {
            source = source.replaceAll("//*", File.separator);
        }
        return source;
    }

    private final String rootDirectory;

    // Se l'header è nel formato valio allora ne estraggo il filename, lo conformo e restituisco il path assoluto
    public String GetAbsolutePath(String header) {
        var isValidHeader = evaluateGetPattern.test(header);
        if (isValidHeader) {
            var relativePath = ConformWithFileSeparator(header.split(" ")[1]);
            return String.format("%s%s", rootDirectory, relativePath);
        }
        else {
            return null;
        }
    }

    // Si controlla che non vi sia già un'istanza di HeaderManipulation, quindi si assegna il path della root directory assicurandoci che sia valido
    public HeaderManipulation(String rootDirectoryPath) throws NullPointerException, SecurityException, IllegalArgumentException {
        if (HeaderManipulation.instance != null) { throw new IllegalStateException("An instance of this class already exists"); }

        if (new File(rootDirectoryPath).isDirectory()) {
            HeaderManipulation.instance = this;
            this.rootDirectory = rootDirectoryPath;
        }
        else {
            throw new IllegalArgumentException("Invalid path: not a directory");
        }
    }
}