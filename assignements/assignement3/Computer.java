package com.albertomartino.assignements.assignement3;

// rappresenta un computer del laboratorio
public class Computer {
    private final int id;
    private int nThesisWritersWaiting;  // il numero di tesisti in attesa su questo computer
    public int getId() {return id;}
    public boolean AreThesisWritersWaiting() {return nThesisWritersWaiting > 0;}
    public void AddThesisWriterWaiting() {nThesisWritersWaiting++;}
    public void RemoveThesisWriterWaiting() {nThesisWritersWaiting--;}
    public Computer(int id) {this.id = id;}
}
