package com.albertomartino.assignements.assignement3;

// Si occupa di istanziare i computer assegnando ad ognuno l'id
// inoltre, fornisce alcune shortcut.
// Per semplicità, ho fatto le seguenti scelte:
// - non si è prestata attenzione a come un utente utilizza l'unità contesa (PC / PCs)
// - si suppone che sia nota, in particolare al tutor, la corrispondenza somePc.id == computers.indexOf(somePc)
public class Lab {
    private final Computer[] computers;

    public Computer[] getComputers() {return computers;}
    public Computer getComputer(int index) {return computers[index];}
    public int getNumberOfComputers() {return computers.length;}

    public Lab(int numberOfComputers) {
        computers = new Computer[numberOfComputers];
        for (int i = 0; i < computers.length; i++) {
            computers[i] = new Computer(i);
        }
    }
}
