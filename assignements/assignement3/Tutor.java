package com.albertomartino.assignements.assignement3;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

// Coordina gli accessi al laboratorio da parte degli utenti
public class Tutor {
    private final Lab lab;
    private final ReentrantLock computersLock; // unica lock per garantire atomicità delle op su dati condivisi
    private final Condition studentReady, thesisReady, teacherReady; // vi si sospenderanno i rispettivi utenti
    private int nTeachersWaiting, nThesisWritersWaiting, nStudentsWaiting; // contano i rispettivi utenti in attesa
    private final List<Integer> freeComputersIndexes; // tutti gli indici delle postazioni libere
    private final String[] labLogs; // utilizzata per stampare l'occupazione di ogni postazione
    private String[] oldLabLogs; // utilizzata per tenere memoria dell'ultimo lablogs
    private final static String freeComputerLog = "---"; // utilizzate per indicare una postazione libera
    private boolean isAvailableToNotify = true; // se il sistema fallisce, il tutor non segnalerà nessun utente

    public Tutor(Lab lab) {
        this.lab = lab;

        // Inizialmente ogni postazione è libera
        freeComputersIndexes = new ArrayList<>(lab.getNumberOfComputers());
        labLogs = new String[lab.getNumberOfComputers()];
        for (int i = 0; i < lab.getNumberOfComputers(); i++) {
            freeComputersIndexes.add(i);
            labLogs[i] = freeComputerLog;
        }
        oldLabLogs = Arrays.copyOf(labLogs, labLogs.length);

        // Istanzio lock e condizioni
        computersLock = new ReentrantLock();
        studentReady = computersLock.newCondition();
        thesisReady = computersLock.newCondition();
        teacherReady = computersLock.newCondition();
    }

    public boolean AreAllComputersFree() {return freeComputersIndexes.size() == lab.getNumberOfComputers();}

    public Computer[] AcquireStudentComputer() throws Exception {
        Computer chosen;

        //tento di acquisire la lock
        try {
            computersLock.lockInterruptibly();
        }
        catch (InterruptedException exception) {
            isAvailableToNotify = false;
            throw exception;
        }

        // finché ci sono docenti in attesa oppure non ci sono computer liberi che non siano attesi da tesisti attendo
        // se vengo interrotto, disabilito la funzionalità di notify del Tutor, rilascio la lock e rimando l'eccezione
        while (nTeachersWaiting > 0 || (chosen = FindStudentAvailableComputer()) == null) {
            nStudentsWaiting++;
            try {
                studentReady.await();
            }
            catch (InterruptedException exception) {
                isAvailableToNotify = false;
                computersLock.unlock();
                throw exception;
            }
            finally {
                nStudentsWaiting--;
            }
        }

        // Rimuovo il computer ottenuto dalla lista di quelli liberi, aggiorno il log, stampo lo stato
        var choosenId = chosen.getId();
        freeComputersIndexes.remove((Integer)choosenId);
        labLogs[choosenId] = String.format("s%d", Thread.currentThread().getId());
        PrintState();

        computersLock.unlock();

        return new Computer[] {chosen};
    }
    private Computer FindStudentAvailableComputer() {
        for (var freeComputerIndex : freeComputersIndexes) {
            var pc = lab.getComputer(freeComputerIndex);
            if (!pc.AreThesisWritersWaiting()) {
                return pc;
            }
        }

        return null;
    }
    public Computer[] AcquireThesisWriterComputer(int thesisIndex) throws Exception {
        var thesisPc = lab.getComputer(thesisIndex);

        //tento di acquisire la lock
        try {
            computersLock.lockInterruptibly();
        }
        catch (InterruptedException exception) {
            isAvailableToNotify = false;
            throw exception;
        }

        // finché ci sono docenti in attesa oppure il computer che voglio è occupato, attendo
        // se vengo interrotto, disabilito la funzionalità di notify del Tutor, rilascio la lock e rimando l'eccezione
        while (nTeachersWaiting > 0 || !freeComputersIndexes.contains(thesisIndex)) {
            nThesisWritersWaiting++;
            thesisPc.AddThesisWriterWaiting();
            try {
                thesisReady.await();
            }
            catch (InterruptedException exception) {
                isAvailableToNotify = false;
                computersLock.unlock();
                throw exception;
            }
            finally {
                nThesisWritersWaiting--;
                thesisPc.RemoveThesisWriterWaiting();
            }
        }

        // Rimuovo il computer ottenuto dalla lista di quelli liberi, aggiorno il log, stampo lo stato
        freeComputersIndexes.remove((Integer)thesisIndex);
        labLogs[thesisIndex] = String.format("t%d", Thread.currentThread().getId());
        PrintState();

        computersLock.unlock();

        return new Computer[] {thesisPc};
    }
    public Computer[] AcquireTeacherComputers() throws Exception {
        //tento di acquisire la lock
        try {
            computersLock.lockInterruptibly();
        }
        catch (InterruptedException exception) {
            isAvailableToNotify = false;
            throw exception;
        }

        // finché c'è almeno un computer occupato, attendo
        // se vengo interrotto, disabilito la funzionalità di notify del Tutor, rilascio la lock e rimando l'eccezione
        while (!AreAllComputersFree()) {
            nTeachersWaiting++;
            try {
                teacherReady.await();
            }
            catch (InterruptedException exception) {
                isAvailableToNotify = false;
                computersLock.unlock();
                throw exception;
            }
            finally {
                nTeachersWaiting--;
            }
        }

        // Svuoto la lista dei computer liberi, aggiorno il log, stampo lo stato
        freeComputersIndexes.clear();
        for (int i = 0; i < labLogs.length; i++) {  labLogs[i] = String.format("T%d", Thread.currentThread().getId()); }
        PrintState();

        computersLock.unlock();

        return Arrays.copyOf(lab.getComputers(), lab.getNumberOfComputers());
    }

    public void Release(Computer[] computers) {
        computersLock.lock();

        // Aggiungo i computer rilasciate alla lista dei computer disponibili
        for (Computer computer : computers) {
            var computerIndex = computer.getId();
            freeComputersIndexes.add(computerIndex);
            labLogs[computerIndex] = freeComputerLog;
        }

        // Stampo lo stato corrente e segnalo chi di dovere
        PrintState();
        Signal();
        computersLock.unlock();
    }

    private void Signal() {
        if (isAvailableToNotify) {
            // se ci sono docenti in attesa e tutti i pc sono liberi risveglio un docente
            if (nTeachersWaiting > 0 && AreAllComputersFree()) {
                teacherReady.signal();
            }
            // altrimenti, risveglio tutti gli altri utenti
            else {
                thesisReady.signalAll();
                studentReady.signalAll();
            }
        }
    }
    public void PrintState() {
        System.out.printf(
                "\nWAITING] Teachers (T): %d\tThesisWriters (t): %d\tStudents (s): %d\n" +
                "\t\t* = changed\t\t$ = some thesis writer is waiting\nLAB] ",
                nTeachersWaiting, nThesisWritersWaiting, nStudentsWaiting
        );
        StringBuilder log = new StringBuilder();
        for (int i = 0; i <labLogs.length; i++) {
            if (lab.getComputer(i).AreThesisWritersWaiting()) {
                log.append("$");
            }
            var labLog = labLogs[i];
            log.append(String.format("[%s]", labLog));
            var isChanged = !labLogs[i].equals(oldLabLogs[i]);
            if (isChanged) {
                log.append("* ");
            }
            else {
                log.append(" ");
            }
        }
        System.out.printf("%s\n", log.toString());
        oldLabLogs = Arrays.copyOf(labLogs, labLogs.length);
    }

}
