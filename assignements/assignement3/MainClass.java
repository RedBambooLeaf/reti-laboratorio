package com.albertomartino.assignements.assignement3;
import com.albertomartino.utilities.Random;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

// Si occupa di istanziare i componenti del sistema con i parametri forniti da linea di comando
// quindi attende che tutti gli utenti abbiano effettuato tutti gli accessi
public class MainClass {
    private final static int numberOfComputers = 20;

    public static void main(String[] args) {
        // Prendo i dati richiesti, se viene lanciata un'eccezione durante lo scan degli input
        // assegno dei valori di default
        var scanner = new Scanner(System.in);
        int nStudents, nTeachers, nThesisWriters;
        try {
            System.out.print("Insert the number of students: ");
            nStudents = scanner.nextInt();
            System.out.print("Insert the number of teachers: ");
            nTeachers = scanner.nextInt();
            System.out.print("Insert the number of thesis writers: ");
            nThesisWriters = scanner.nextInt();
        }
        catch (Exception exception) {
            System.out.println("si è verificato un errore, sono stati impostati valori di default.");
            exception.printStackTrace();
            nStudents = 200;
            nThesisWriters = 24;
            nTeachers = 6;
        }
        finally {
            scanner.close();
        }

        // Creo laboratorio, tutore e un threadPool col quale eseguo gli utenti generati
        var lab = new Lab(numberOfComputers);
        var tutor = new Tutor(lab);
        var executor = Executors.newCachedThreadPool();
        GenerateUsers(() -> new Student(tutor), executor, nStudents);
        GenerateUsers(() -> new ThesisWriter(tutor, Random.Between(0, numberOfComputers)), executor, nThesisWriters);
        GenerateUsers(() -> new Teacher(tutor), executor, nTeachers);

        // il seguente codice commentato serve a testare cosa succede nel caso di interruzione:
        // il tutor dovrebbe essere in grado di riportare il lab allo stato di occupazione iniziale (tutto libero)
/*
        try {
            Thread.sleep(Random.Between(0L, 10000L));
            executor.shutdownNow();
            tutor.PrintState();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
*/

        // Attendo che tutti gli utenti abbiano terminato
        try {
            WaitForUsersToFinish(executor);
        }
        catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    // Tramite l'interfaccia rendo generico il metodo di generazione degli utenti
    private interface InstanceOfUser {User Create ();}
    private static void GenerateUsers(InstanceOfUser instance, Executor executor, int number) {
        for (int i = 0; i < number; i++) {
            executor.execute(instance.Create());
        }
    }

    // Un semplice metodo per iniziare lo shutdown quindi attendere la terminazione dei thread utente
    private static void WaitForUsersToFinish(ExecutorService executor) throws InterruptedException {
        executor.shutdown();
        while (!executor.isTerminated()) {
            executor.awaitTermination(1, TimeUnit.SECONDS);
        }
    }
}
