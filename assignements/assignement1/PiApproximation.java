package com.albertomartino.assignements.assignement1;

public class PiApproximation implements Runnable {
    private double accuracy;
    private double pi;

    public double GetPi() {
        return pi;
    }

    public PiApproximation(double accuracy) {
        this.accuracy = accuracy;
        pi = 0.0;
    }

    // il loop viene eseguito finché questo thread non viene interrotto
    // oppure quando l'accuratezza raggiunta è soddisfacente
    @Override
    public void run() {
        pi = 0.0;
        var iteration = 0;
        while (!Thread.interrupted()) {
            var increment = 4.0 / (2 * iteration + 1);

            pi += iteration++ % 2 == 0 ? increment : -increment;

            if (Math.abs(pi - Math.PI) < accuracy) {
                return;
            }
        }
    }

}
