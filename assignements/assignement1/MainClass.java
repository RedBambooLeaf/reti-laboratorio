package com.albertomartino.assignements.assignement1;

// IPOTESI
// accuracy grade:          da 1 a 15 indica quanto deve essere accurata l'approsimazione;
//                          l'approssimazione pi è accettabile se abs(pi - Math.PI) < accuracy^(-10)
// timeout seconds:         il timeout si è inteso da specificare in secondi, minimo 1 secondo
public class MainClass {

    private static final String usage = "usage: MainClass <accuracy grade> <timeout seconds>";

    public static void main(String[] args) {

        // Prendo gli input e verifico che siano validi, altrimenti ritorno comunicando
        // un messaggio d'errore e un messaggio di usage
        Inputs inputs = GetInputs(args);
        var areInputsInvalid = inputs == null;
        if (areInputsInvalid) {
            System.out.println(usage);
            return;
        }

        // Creo un'istanza di PiApproximation per l'approssimazione di PI
        var piApproximation = new PiApproximation(inputs.accuracy);

        // Lancio piApproximation su un thread demone creato appositamente
        var piApproximationThread = new Thread(piApproximation);
        piApproximationThread.setDaemon(true);
        piApproximationThread.start();

        // Concedo un tempo limitato al demone per terminare
        // Gestisco eventuali interruzioni al Thread main
        try {
            piApproximationThread.join(inputs.waitInMillis);
            piApproximationThread.interrupt();
        }
        catch (InterruptedException exception) {
            System.out.println("Main thread interrupted.");
            exception.printStackTrace();
            return;
        }

        // Se il demone è ancora vivo, significa che non ha terminato nel tempo consentito,
        // altrimenti un'approssimazione di PI accettabile è stata calcolata.
        // Comunico l'esito all'utente.
        if (!piApproximationThread.isAlive()) {
            var approximatedPi = piApproximation.GetPi();
            System.out.printf("PI: %.15f\n", approximatedPi);
        }
        else {
            System.out.println("Time out! Try again with more time.");
        }
    }

    private static Inputs GetInputs(String[] args) {

        if (args.length != 2) {
            System.out.println("ERROR: invalid arguments.");
            return null;
        }

        int accuracyGrade;
        int waitInSeconds;
        try {
            accuracyGrade = Integer.parseInt(args[0]);
            waitInSeconds = Integer.parseInt(args[1]);
        }
        catch (NumberFormatException exception) {
            System.out.println("ERROR: both inputs are required to be integers.");
            exception.printStackTrace();
            return null;
        }

        if (accuracyGrade <= 0 || accuracyGrade > 15) {
            System.out.println("ERROR: Accuracy grade is required to be in the interval [1, 15].");
            return null;
        }

        if (waitInSeconds <= 0) {
            System.out.println("ERROR: timeout is required to be a positive natural number.");
            return null;
        }

        return new Inputs(Math.pow(10, -1 * accuracyGrade), waitInSeconds * 1000L);
    }

    private static class Inputs {
        public double accuracy;
        public long waitInMillis;

        public Inputs(double accuracy, long waitInMillis) {
            this.accuracy = accuracy;
            this.waitInMillis = waitInMillis;
        }
    }
}
