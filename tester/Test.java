package com.albertomartino.tester;

import com.albertomartino.utilities.Random;

public class Test {
    public static void main(String[] args) {

    }

    private static void PrintRandomNumbers() {
        System.out.println("Random doubles:");
        for (int i = 0; i < 10; i++) {
            System.out.println(Random.Between(0, 10.00));
        }

        System.out.println("\nRandom floats:");
        for (int i = 0; i < 10; i++) {
            System.out.println(Random.Between(0, 10f));
        }

        System.out.println("\nRandom ints:");
        for (int i = 0; i < 10; i++) {
            System.out.println(Random.Between(0, 10));
        }
    }
}
