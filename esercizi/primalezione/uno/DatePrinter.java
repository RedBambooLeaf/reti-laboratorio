package com.albertomartino.esercizi.primalezione.uno;

import java.util.Calendar;

public class DatePrinter {
    public static void main(String[] args) {
        var printer = new DatePrinter();
        printer.PrintDateTimeForever();
    }

    public void PrintDateTimeForever() {
        while (true) {
            var thread = Thread.currentThread();
            var calendar = Calendar.getInstance();
            var time = calendar.getTime();
            System.out.println(thread.getName());
            System.out.println(time);

            try {
                Thread.sleep(2000);
            }
            catch (InterruptedException exception) {
                System.out.println("Thread interrupted");
            }
        }
    }
}
