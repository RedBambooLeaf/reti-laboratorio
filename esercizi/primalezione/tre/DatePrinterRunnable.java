package com.albertomartino.esercizi.primalezione.tre;
import com.albertomartino.esercizi.primalezione.uno.DatePrinter;

public class DatePrinterRunnable implements Runnable {
    DatePrinter datePrinter;

    public DatePrinterRunnable() {
        datePrinter = new DatePrinter();
    }

    public static void main(String[] args) {
        var datePrinterRunnable = new DatePrinterRunnable();
        var thread = new Thread(datePrinterRunnable);
        thread.start();
    }

    @Override
    public void run() {
        datePrinter.PrintDateTimeForever();
    }
}
