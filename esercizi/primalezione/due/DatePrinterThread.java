package com.albertomartino.esercizi.primalezione.due;

import com.albertomartino.esercizi.primalezione.uno.DatePrinter;

public class DatePrinterThread extends Thread {
    DatePrinter datePrinter;

    public  DatePrinterThread() {
        datePrinter = new DatePrinter();
    }

    public static void main(String[] args) {
        var mainThreadName = Thread.currentThread().getName();
        System.out.println(mainThreadName);
        var thread = new DatePrinterThread();
        thread.start();
    }

    @Override
    public void run() {
        datePrinter.PrintDateTimeForever();
    }
}
