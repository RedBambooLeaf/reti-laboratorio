package com.albertomartino.esercizi.sync;

import com.albertomartino.utilities.Random;
import com.albertomartino.utilities.Threads;

public class Account implements Runnable {
    private  Wallet wallet;
    private int amount;
    private boolean isAdding;

    public Account(Wallet wallet, boolean isAdding, int amount) {
        this.wallet = wallet;
        this.isAdding = isAdding;
        this.amount = amount;
    }

    @Override
    public void run() {
        System.out.printf("Account %d started.\n", Thread.currentThread().getId());
        if (isAdding) {
            wallet.Add(amount);
        }
        else {
            wallet.Remove(amount);
        }
    }
}
