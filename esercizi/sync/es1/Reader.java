package com.albertomartino.esercizi.sync.es1;

public class Reader implements Runnable {
    private final Counter counter;

    public Reader(Counter counter) {
        this.counter = counter;
    }

    @Override
    public void run() {
        var count = counter.get();
/*
        System.out.printf("Count: %d\n", count);
*/
    }
}
