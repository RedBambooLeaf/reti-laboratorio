package com.albertomartino.esercizi.sync.es1;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainClass {
    private static final int numberOfParticipantsPerPart = 1000;
    private static final int iterations = 100;

    public static void main(String[] args) {
        double[] betterAmountSum = new double[iterations];

        for (int i = 0; i < iterations; i++) {
            var safeCounterTime = TestCounter(new SafeCounter());
            var bidirectionalSafeCounterTime = TestCounter(new BidirectionalSafeCounter());

            var fasterName = safeCounterTime < bidirectionalSafeCounterTime ? "SafeCounter" : "R/WSafeCounter";
            var slowerName = safeCounterTime < bidirectionalSafeCounterTime ? "R/WSafeCounter" : "SafeCounter";
            var faster = Math.min(safeCounterTime, bidirectionalSafeCounterTime);
            var slower = Math.max(safeCounterTime, bidirectionalSafeCounterTime);
            double betterAmount = ((int)((slower / faster) * 100)) / 100.0;
            betterAmountSum[i] = betterAmount;

/*
            System.out.printf(
                    "Safe Counter Time:________________________%.4f\n" +
                            "R/W Safe Counter Time:__________%.4f\n" +
                            "%s is\tx%.2f\tfaster than\t%s\n",
                    safeCounterTime, bidirectionalSafeCounterTime, fasterName, betterAmount, slowerName
            );
*/
        }

        var diff = 0.0;
        var sum = 0.0;
        for (int i = 0; i < betterAmountSum.length; i++) {
            var localDiff = 0;
            sum += betterAmountSum[i];
            for (int j = i + 1; j < betterAmountSum.length; j++) {
                localDiff += Math.abs(betterAmountSum[i] - betterAmountSum[j]);
            }
            diff += localDiff / (betterAmountSum.length - i);
        }
        System.out.printf("Main iterations: %d\tNumOfPart: %d\n", iterations, numberOfParticipantsPerPart);
        System.out.printf("Deviance: %f\n", diff / betterAmountSum.length);
        System.out.printf("RW is\tx%.2f\tfaster than\tDEFAULT\n", sum / betterAmountSum.length);

    }

    public static <T extends Counter> double TestCounter(T counter) {
        var startTime = System.currentTimeMillis();
        var executor = Executors.newCachedThreadPool();
        for (int i = 0; i < numberOfParticipantsPerPart; i++) {
            executor.execute(new Writer(counter));
        }
        for (int i = 0; i < numberOfParticipantsPerPart; i++) {
            executor.execute(new Reader((counter)));
        }

        executor.shutdown();
        try {
            while (!executor.isTerminated())
                executor.awaitTermination(1, TimeUnit.SECONDS);
        }
        catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        var endTime = System.currentTimeMillis();
        return (endTime - startTime)/1000.0;
    }
}
