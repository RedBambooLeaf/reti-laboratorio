package com.albertomartino.esercizi.sync.es1;

import com.albertomartino.utilities.Threads;

public class Writer implements  Runnable {
    private final Counter counter;

    public Writer(Counter counter) {
        this.counter = counter;
    }

    @Override
    public void run() {
        counter.increment();
    }
}
