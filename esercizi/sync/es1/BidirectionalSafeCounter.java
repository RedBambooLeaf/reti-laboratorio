package com.albertomartino.esercizi.sync.es1;

import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class BidirectionalSafeCounter extends Counter {
    private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    @Override
    public void increment() {
        lock.writeLock().lock();
        super.increment();
        lock.writeLock().unlock();
    }

    @Override
    public int get() {
        lock.readLock().lock();
        var res = super.get();
        lock.readLock().unlock();
        return res;
    }
}
