package com.albertomartino.esercizi.sync.es1;

import java.util.concurrent.locks.ReentrantLock;

public class SafeCounter extends Counter {
    private ReentrantLock lock = new ReentrantLock();

    @Override
    public void increment() {
        lock.lock();
        super.increment();
        lock.unlock();
    }

    @Override
    public int get() {
        lock.lock();
        var res = super.get();
        lock.unlock();
        return  res;
    }
}
