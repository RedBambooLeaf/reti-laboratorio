package com.albertomartino.esercizi.sync;

import com.albertomartino.utilities.Threads;

import java.util.concurrent.locks.ReentrantLock;

public class Wallet {
    private ReentrantLock lock;
    private int balance;
    private final static int minBalance = 0;
    private final static int maxBalance = 999999;
    private final static long minWait = 0, maxWait = 50;

    public int getBalance() {return balance;}

    public Wallet(int initialBalance) {
        lock = new ReentrantLock();
        balance = initialBalance;
    }

    public void Add(int amount) {
        amount = amount < 0 ? 0 : amount;
        ChangeBalance(amount);
    }

    public void Remove(int amount) {
        amount = amount < 0 ? 0 : amount;
        ChangeBalance(-amount);
    }

    private void ChangeBalance(int newBalance) {
        try {
            lock.lockInterruptibly();
        }
        catch (InterruptedException exception) {
            System.out.printf("%d) I dint't catch the lock (%d)!\n", Thread.currentThread().getId(), newBalance);
            return;
        }

        newBalance += balance;
        if (newBalance < minBalance) {
            newBalance = minBalance;
        }
        else if (newBalance > maxBalance) {
            newBalance = maxBalance;
        }

        try {
            Threads.RandomSleep(minWait, maxWait);
            balance = newBalance;
        }
        catch (InterruptedException exception) {
            System.out.printf("%d) interrupted while keeping lock\n", Thread.currentThread().getId());
        }
        finally {
            lock.unlock();
        }
    }
}
