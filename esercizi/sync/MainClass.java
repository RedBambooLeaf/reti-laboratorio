package com.albertomartino.esercizi.sync;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainClass {
    private static final int numberOfAccounts = 50;

    public static void main(String[] args) {
        var executor = Executors.newCachedThreadPool();
        var wallet = new Wallet(50000);
        for (int i = 0; i < numberOfAccounts; i++) {
            var account = new Account(wallet, i % 2 == 0, 100);
            executor.execute(account);
        }

        executor.shutdown();
        try {
            executor.awaitTermination(1, TimeUnit.MINUTES);
        }
        catch (InterruptedException ex) {
            ex.printStackTrace();
            return;
        }

        System.out.printf("Balance: %d", wallet.getBalance());
    }
}
