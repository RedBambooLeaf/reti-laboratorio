package com.albertomartino.esercizi.monitor;

public class Consumer implements Runnable {
    private final boolean isConsumingEven;
    private final Dropbox dropbox;

    public Consumer(boolean isConsumingEven, Dropbox dropbox) {
        this.isConsumingEven = isConsumingEven;
        this.dropbox = dropbox;
    }

    @Override
    public void run() {
        while (true) {
            dropbox.take(isConsumingEven);
        }
    }
}
