package com.albertomartino.esercizi.monitor;

public class DropboxMonitored extends Dropbox {
    @Override
    public synchronized int take(boolean e) {
        while (!full || e ^ (num % 2 == 0)) {
            try {
                wait();
            }
            catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
        var result = super.take(e);
        notifyAll();
        return result;
    }

    @Override
    public synchronized void put(int n) {
        while (full) {
            try {
                wait();
            }
            catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
        super.put(n);
        notifyAll();
    }

/*    public synchronized void StartRead() {
        while (writing) {
            try {
                wait();
            }
            catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }

        readers++;
    }
    public synchronized void EndRead(){
        readers--;
        if (readers == 0) {
            notifyAll();
        }
    }

    public synchronized void StartWrite() {
        while (writing || readers != 0) {
            try {
                wait();
            }
            catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
        writing = true;
    }
    public synchronized void EndWrite(){
        writing = false;
        notifyAll();
    }*/
}
