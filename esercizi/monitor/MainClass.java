package com.albertomartino.esercizi.monitor;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainClass {
    public static void main(String[] args) {
        var dropbox = new Dropbox();
        var consumerEven = new Consumer(true, dropbox);
        var consumerOdd = new Consumer(false, dropbox);
        var producer = new Producer(dropbox);

        var executor = Executors.newCachedThreadPool();
        executor.execute(consumerEven);
        executor.execute(consumerOdd);
        executor.execute(producer);

        executor.shutdown();
        while (!executor.isTerminated()) {
            try {
                executor.awaitTermination(1, TimeUnit.SECONDS);
            }
            catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
    }
}
