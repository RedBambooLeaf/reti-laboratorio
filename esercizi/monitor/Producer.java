package com.albertomartino.esercizi.monitor;

import com.albertomartino.utilities.Random;

public class Producer implements Runnable {
    private final Dropbox dropbox;

    public Producer(Dropbox dropbox) {
        this.dropbox = dropbox;
    }

    @Override
    public void run() {
        while (true) {
            dropbox.put(Random.Between(0, 100));
        }
    }
}
