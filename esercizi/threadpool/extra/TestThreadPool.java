package com.albertomartino.esercizi.threadpool.extra;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TestThreadPool {
    public static void main(String[] args) {
        var executorService = Executors.newFixedThreadPool(5);//creating a pool of 5 threads

        for (int i = 0; i < 10; i++) {
            var worker = new WorkerThread("" + i);
            executorService.execute(worker);//calling execute method of ExecutorService
        }

        try  {
            executorService.awaitTermination(99999, TimeUnit.SECONDS);
        }
        catch (InterruptedException ex) {
            System.out.println("oioia");
        }
        System.out.println("Finished all threads");
    }
}