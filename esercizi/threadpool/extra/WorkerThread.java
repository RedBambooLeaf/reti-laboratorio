package com.albertomartino.esercizi.threadpool.extra;

class WorkerThread implements Runnable {
    private String message;
    public WorkerThread(String s){
        this.message=s;
    }
    public void run() {
        System.out.println(Thread.currentThread().getName()+" (Start) message = "+message);
        try  {
            processmessage();
            return;
        }
        catch (InterruptedException ex) {
            System.out.println("Interrupted");
        }
        System.out.println(Thread.currentThread().getName()+" (End)");//prints thread name
    }
    private void processmessage() throws InterruptedException {
        try {  Thread.sleep(2000);  } catch (InterruptedException e) { throw e; }
    }
}
