package com.albertomartino.esercizi.threadpool.uno;

import com.albertomartino.utilities.Threads;

public class Viaggiatore implements Runnable {
    private int id;
    private double minSleep, maxSleep;

    public Viaggiatore(int id, double minSleep, double maxSleep) {
        this.id = id;
        this.minSleep = minSleep;
        this.maxSleep = maxSleep;
    }

    @Override
    public void run() {
        System.out.println(String.format("Viaggiatore %d: sto acquistando un biglietto", id));

        try {
            Threads.RandomSleep((long)minSleep, (long) maxSleep);
        }
        catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        System.out.println(String.format("Viaggiatore %d: ho acquistato il biglietto", id));
    }

}
