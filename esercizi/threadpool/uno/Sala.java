package com.albertomartino.esercizi.threadpool.uno;

import com.albertomartino.utilities.Threads;

import java.util.concurrent.*;

public class Sala {
    private static  final  int numberOfEmettitrici = 5;
    private static  final  int capienzaSala = 10;
    private static  final  int numberOfViaggiatori = 50;
    private static  final  double minSleep = 2000;
    private static  final  double maxSleep = 4000;

    public static void main(String[] args) {
        var queue = new ArrayBlockingQueue<Runnable>(capienzaSala);
        var viaggiatori = new ThreadPoolExecutor(
                numberOfEmettitrici,
                numberOfEmettitrici,
                0L,
                TimeUnit.MILLISECONDS,
                queue);

        for (int i = 0; i < numberOfViaggiatori; i++) {

            try {
                viaggiatori.execute(new Viaggiatore(i, minSleep, maxSleep));
            }
            catch (RejectedExecutionException ex) {
                System.out.println(String.format("Viaggiatore %d: sala esaurita", i));
            }

            try  {
                Thread.sleep(50);
            }
            catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        viaggiatori.shutdown();
    }
}
