package com.albertomartino.esercizi.threadpool.power;

import java.util.ArrayList;
import java.util.concurrent.*;

public class MainPower {
    public static void main(String[] args) {

        var pool = Executors.newCachedThreadPool();
        var results = new ArrayList<Future<Integer>>(50);
        for (int i = 2; i <= 50; i++) {
            var exp = new Power(2, i);
            results.add(pool.submit(exp));
        }

        var sum = 0;
        for (var el : results) {
            try {
                sum += el.get(1000L, TimeUnit.MILLISECONDS);
            }
            catch (InterruptedException ex) {
                ex.printStackTrace();
                return;
            }
            catch (TimeoutException ex2) {
                System.out.println("Time out.");
                return;
            }
            catch (ExecutionException ex3) {
                System.out.printf("oisiosj");
                return;
            }
        }

        System.out.printf("Power is: %d", sum);
        pool.shutdown();
    }
}
