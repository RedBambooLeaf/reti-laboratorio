package com.albertomartino.esercizi.threadpool.power;

import java.util.concurrent.Callable;

public class Power implements Callable<Integer> {

    private int base;
    private  int exp;

    public Power(int base, int exp) {
        this.base = base;
        this.exp = exp;
    }

    @Override
    public Integer call() throws Exception {
        System.out.printf("Esecuzione %d^%d in %d", base, exp, Thread.currentThread().getId());
        return (int)Math.pow(base, exp);
    }

}
